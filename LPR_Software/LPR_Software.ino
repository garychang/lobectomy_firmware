#include <Servo.h>
Servo ServoVent, ServoIntake, ServoUpper, ServoMiddle, ServoLower;
const int SugeryModePin = 2;
const int LungUpperPin = 3;
const int LungMiddlePin = 5;
const int LungLowerPin = 4;
const int RedLed = 22;
const int BlueLed = 24;
const int YellowLed = 26;
const int GreenLed = 28;
unsigned long  SugeryFinishedTime;
boolean NormalModeInitialBlowFlag = false;
boolean NormalModeIdelFlag = false;
unsigned long previousTime = millis();
int NormalModePeriod = 7000;
int SugeryModeVentTime;
int SugeryModeIntakeTime;
int NormalModePreparationChargeTime;
int NormalModePuaseTime;
int NormalModeVentTime;
int LungUpperOnVentTime;
int LungUpperOffVentAndOnIntakeTime;
int LungUpperOffIntakeTime;
int LungUpperChargeTime;
int LungMiddleOnVentTime;
int LungMiddleOffVentAndOnIntakeTime;
int LungMiddleOffIntakeTime;
int LungMiddleChargeTime;
int LungLowOnVentTime;
int LungLowOffVentAndOnIntakeTime;
int LungLowOffIntakeTime;
int LungLowChargeTime;
String MachineState;

/*
	控制肺葉進吐氣的閥門與呼吸機構的時間關係圖

	呼吸機構State Time point		0---------------------NormalModeVentTime---------------------NormalModePuaseTime------------------NormalModePeriod
								|							|										 |									   |
	呼吸機構State					|---------vent--------------|-------------idling---------------------|--------------Intake-----------------|
								|							|										 |									   |
	Valve State Time point		0-------------VentOn--------------------VentOffAndIntakeOn---------------------IntakeOff----------NormalModePeriod
								|				|								|					 			   |					   |
	Valve State					|-----On--------|---------off-------------------|------------------On--------------|-----------Off---------|
*/

//----------------setup-------------------
void setup() {
	Serial.begin(115200);
	pinMode(RedLed, OUTPUT);
	pinMode(BlueLed, OUTPUT);
	pinMode(YellowLed, OUTPUT);
	pinMode(GreenLed, OUTPUT);
	pinMode(SugeryModePin, INPUT);
	pinMode(LungUpperPin, INPUT);
	pinMode(LungMiddlePin, INPUT);
	pinMode(LungLowerPin, INPUT);
	ServoIntake.attach(8);//0 degree is pumping from air  ,90 degree is pumping from to the system
	ServoVent.attach(9);//0 degree is exhausting to the system ,100 degree is exhausting to air
	ServoUpper.attach(10);//0度為On,90度為off
	ServoMiddle.attach(11);//0度為On,90度為off
	ServoLower.attach(12);//0度為On,90度為off
}

//----------------Servo Switch Funtion--------------------
void ServoSwitch(String ServoNumber, String ServoState)
{
	if (ServoNumber == "ServoBreath")
	{
		if (ServoState == "Exhausting")
		{
			ServoIntake.write(0);
			ServoVent.write(0);
		}
		else if (ServoState == "Pumping")
		{
			ServoIntake.write(90);
			ServoVent.write(100);
		}
		else if (ServoState == "Idling")
		{
			ServoIntake.write(0);
			ServoVent.write(100);
		}
	}
	if (ServoNumber == "ServoUpper")
	{
		if (ServoState == "On")
		{
			ServoUpper.write(0);
		}
		else
		{
			ServoUpper.write(90);
		}
	}
	if (ServoNumber == "ServoMiddle")
	{
		if (ServoState == "On")
		{
			ServoMiddle.write(0);
		}
		else
		{
			ServoMiddle.write(90);
		}
	}
	if (ServoNumber == "ServoLower")
	{
		if (ServoState == "On")
		{
			ServoLower.write(0);
		}
		else
		{
			ServoLower.write(90);
		}
	}
}

//----------------Led Funtion--------------------
void Led(String MachineState)
{
	if (MachineState == "SurgeryPreparation")
	{
		digitalWrite(RedLed, HIGH);
		digitalWrite(BlueLed, LOW);
		digitalWrite(YellowLed, LOW);
		digitalWrite(GreenLed, LOW);
	}
	if (MachineState == "SurgeryStart")
	{
		digitalWrite(RedLed, LOW);
		digitalWrite(BlueLed, HIGH);
		digitalWrite(YellowLed, LOW);
		digitalWrite(GreenLed, LOW);
	}
	if (MachineState == "NormalModePreparation")
	{
		digitalWrite(RedLed, LOW);
		digitalWrite(BlueLed, LOW);
		digitalWrite(YellowLed, HIGH);
		digitalWrite(GreenLed, LOW);
	}
	if (MachineState == "NormalModeStart")
	{
		digitalWrite(RedLed, LOW);
		digitalWrite(BlueLed, LOW);
		digitalWrite(YellowLed, LOW);
		digitalWrite(GreenLed, HIGH);
	}
}

//----------------loop--------------------
void loop() {
	boolean  LungLowerSwPin = digitalRead(LungLowerPin);
	boolean  LungMiddleSwPin = digitalRead(LungMiddlePin);
	boolean  LungUpperSwPin = digitalRead(LungUpperPin);
	boolean  SugeryModeSwitchPin = digitalRead(SugeryModePin);
	unsigned long  currentTime = millis();

	//根據三個肺葉開關與否，的8種狀態
	int LungState = LungUpperSwPin * 1 + LungMiddleSwPin * 2 + LungLowerSwPin * 4;//0=all off、1=up、2=mid、3=up&mid、4=low、5=up&low、6=mid&low、7=all on

	//------------------根據三個肺葉開關的8種狀態，撰寫的8個CASE--------------
	switch (LungState)
	{
	case 0:
		Serial.println("all off");
		break;
	case 1:
		SugeryModeVentTime = 20000;
		SugeryModeIntakeTime = 20000;
		NormalModePreparationChargeTime = 2000;
		NormalModeVentTime = 1900;
		NormalModePuaseTime = 4500;
		LungUpperChargeTime = 3000;
		LungMiddleChargeTime = 0;
		LungLowChargeTime = 0;
		LungUpperOnVentTime = 3500;
		LungUpperOffVentAndOnIntakeTime = LungUpperOnVentTime;
		LungUpperOffIntakeTime = NormalModePeriod;
		LungMiddleOnVentTime = 0;
		LungMiddleOffVentAndOnIntakeTime = 3500;
		LungMiddleOffIntakeTime = LungUpperOffVentAndOnIntakeTime;
		LungLowOnVentTime = 0;
		LungLowOffVentAndOnIntakeTime = 3500;
		LungLowOffIntakeTime = LungLowOffVentAndOnIntakeTime;
		Serial.println("up");
		break;
	case 2:
		SugeryModeVentTime = 20000;
		SugeryModeIntakeTime = 20000;
		NormalModePreparationChargeTime = 1800;
		NormalModeVentTime = 1200;
		NormalModePuaseTime = 6000;
		LungUpperChargeTime = 0;
		LungMiddleChargeTime = 3000;
		LungLowChargeTime = 0;
		LungUpperOnVentTime = 0;
		LungUpperOffVentAndOnIntakeTime = 3500;
		LungUpperOffIntakeTime = LungUpperOffVentAndOnIntakeTime;
		LungMiddleOnVentTime = 4000;
		LungMiddleOffVentAndOnIntakeTime = LungMiddleOnVentTime;
		LungMiddleOffIntakeTime = NormalModePeriod;
		LungLowOnVentTime = 0;
		LungLowOffVentAndOnIntakeTime = 3500;
		LungLowOffIntakeTime = LungLowOffVentAndOnIntakeTime;
		Serial.println("mid");
		break;
	case 3:
		SugeryModeVentTime = 20000;
		SugeryModeIntakeTime = 20000;
		NormalModePreparationChargeTime = 2000;
		NormalModeVentTime = 3000;
		NormalModePuaseTime = 3000;
		LungUpperChargeTime = 4000;
		LungMiddleChargeTime = 4000;
		LungLowChargeTime = 0;
		LungUpperOnVentTime = 3000;
		LungUpperOffVentAndOnIntakeTime = 3000;
		LungUpperOffIntakeTime = NormalModePeriod;
		LungMiddleOnVentTime = 2000;
		LungMiddleOffVentAndOnIntakeTime = 5000;
		LungMiddleOffIntakeTime = 6000;
		LungLowOnVentTime = 0;
		LungLowOffVentAndOnIntakeTime = 3500;
		LungLowOffIntakeTime = LungLowOffVentAndOnIntakeTime;
		Serial.println("up&mid");
		break;
	case 4:
		SugeryModeVentTime = 30000;
		SugeryModeIntakeTime = 30000;
		NormalModePreparationChargeTime = 8000;
		NormalModeVentTime = 3000;
		NormalModePuaseTime = 4500;
		LungUpperChargeTime = 0;
		LungMiddleChargeTime = 0;
		LungLowChargeTime = 8000;
		LungUpperOnVentTime = 0;
		LungUpperOffVentAndOnIntakeTime = 3500;
		LungUpperOffIntakeTime = LungUpperOffVentAndOnIntakeTime;
		LungMiddleOnVentTime = 0;
		LungMiddleOffVentAndOnIntakeTime = 3500;
		LungMiddleOffIntakeTime = NormalModePeriod;
		LungLowOnVentTime = 3500;
		LungLowOffVentAndOnIntakeTime = LungLowOnVentTime;
		LungLowOffIntakeTime = NormalModePeriod;
		Serial.println("low");
		break;
	case 5:
		SugeryModeVentTime = 30000;
		SugeryModeIntakeTime = 30000;
		NormalModePreparationChargeTime = 12000;
		NormalModeVentTime = 4000;
		NormalModePuaseTime = 4000;
		LungUpperChargeTime = 4000;
		LungMiddleChargeTime = 0;
		LungLowChargeTime = 12000;
		LungUpperOnVentTime = 2000;
		LungUpperOffVentAndOnIntakeTime = 5000;
		LungUpperOffIntakeTime = 7000;
		LungMiddleOnVentTime = 0;
		LungMiddleOffVentAndOnIntakeTime = 3500;
		LungMiddleOffIntakeTime = LungMiddleOffVentAndOnIntakeTime;
		LungLowOnVentTime = 3000;
		LungLowOffVentAndOnIntakeTime = 3000;
		LungLowOffIntakeTime = NormalModePeriod;
		Serial.println("up&low");
		break;
	case 6:
		SugeryModeVentTime = 30000;
		SugeryModeIntakeTime = 30000;
		NormalModePreparationChargeTime = 11000;
		NormalModeVentTime = 4000;
		NormalModePuaseTime = 4000;
		LungUpperChargeTime = 0;
		LungMiddleChargeTime = 8000;
		LungLowChargeTime = 11000;
		LungUpperOnVentTime = 0;
		LungUpperOffVentAndOnIntakeTime = 3500;
		LungUpperOffIntakeTime = LungUpperOffVentAndOnIntakeTime;
		LungMiddleOnVentTime = 2000;
		LungMiddleOffVentAndOnIntakeTime = 5000;
		LungMiddleOffIntakeTime = 6500;
		LungLowOnVentTime = 3000;
		LungLowOffVentAndOnIntakeTime = 3000;
		LungLowOffIntakeTime = NormalModePeriod;
		Serial.println("mid&low");
		break;
	case 7:
		SugeryModeVentTime = 30000;
		SugeryModeIntakeTime = 30000;
		NormalModePreparationChargeTime = 12000;
		NormalModeVentTime = 4000;
		LungUpperChargeTime = 8000;
		LungMiddleChargeTime = 9000;
		LungLowChargeTime = NormalModePreparationChargeTime;
		LungUpperOnVentTime = 2500;
		LungUpperOffVentAndOnIntakeTime = NormalModeVentTime;
		LungUpperOffIntakeTime = 6500;
		LungMiddleOnVentTime = 2500;
		LungMiddleOffVentAndOnIntakeTime = NormalModeVentTime;
		LungMiddleOffIntakeTime = 6500;
		LungLowOnVentTime = NormalModeVentTime;
		LungLowOffVentAndOnIntakeTime = NormalModeVentTime;
		LungLowOffIntakeTime = NormalModePeriod;
		Serial.println("all on");
		break;
	}

	//------------------根據手術準備模式、手術模式、一般呼吸準備模式、一般呼吸模式四種模式來開關LED------------------
	Led(MachineState);
	Serial.print(MachineState);
	Serial.print("currentTime:");
	Serial.print(currentTime);
	Serial.print("previousTime:");
	Serial.println(previousTime);

	//------------------如果手術模式的開關打開，將肺葉抽到500ML--------------
	if (SugeryModeSwitchPin)
	{
		//------------------如果上肺葉開關開啟，上肺葉氣閥打開--------------
		if (LungUpperSwPin) {
			ServoSwitch("ServoUpper", "On");
			Serial.println("LungUpper On");
		}
		else {
			ServoSwitch("ServoUpper", "Off");
			Serial.println("LungUpper Switch Off");
		}

		//------------------如果中肺葉開關開啟，中肺葉氣閥打開--------------
		if (LungMiddleSwPin) {
			ServoSwitch("ServoMiddle", "On");
			Serial.println("LungMiddle On");
		}
		else {
			ServoSwitch("ServoMiddle", "Off");
			Serial.println("LungMiddle Switch Off");
		}

		//------------------如果下肺葉開關開啟，下肺葉氣閥打開--------------
		if (LungLowerSwPin) {
			ServoSwitch("ServoLower", "On");
			Serial.println("LungLower On");
		}
		else {
			ServoSwitch("ServoLower", "Off");
			Serial.println("LungLower Switch Off");
		}

		//------------------如果時段週期介於0~SugeryModeVentTime(20000ms)之間，先抽氣--------------
		if (0 < currentTime - previousTime && currentTime - previousTime <= SugeryModeVentTime)
		{
			ServoSwitch("ServoBreath", "Pumping");
			NormalModeInitialBlowFlag = false;
			MachineState = "SurgeryPreparation";
			Serial.println("Start Sugerymode Exhaust");
		}

		//------------------如果時段週期介於SugeryModeVentTime(20000ms)~SugeryModeIntakeTime(21500ms)之間，充氣--------------
		else if (SugeryModeVentTime < currentTime - previousTime && currentTime - previousTime <= SugeryModeIntakeTime)
		{
			ServoSwitch("ServoBreath", "Pumping");
			NormalModeInitialBlowFlag = false;
			MachineState = "SurgeryPreparation";
			Serial.println("Start Sugerymode Charge");
		}

		//------------------如果時段週期超過SugeryModeIntakeTime(21500ms)後，令馬達空轉--------------
		else
		{
			ServoSwitch("ServoBreath", "Pumping");
			NormalModeInitialBlowFlag = true;
			MachineState = "SurgeryStart";
			
			Serial.println("Start Sugerymode idling");
			Serial.print("SugeryFinishedTime:");
			Serial.println(SugeryFinishedTime);
		}
		SugeryFinishedTime = millis();
	}

	//------------------如果手術模式的開關關閉，開啟一般呼吸模式--------------
	else {
		//------------------如果上肺葉開關開啟，上肺葉氣閥隨著不同的狀態開關閥--------------
		if (LungUpperSwPin) {
			//------------------充氣準備模式下，上肺葉開關狀態--------------
			if (NormalModeInitialBlowFlag)
			{
				//------------------開筏至LungUpperChargeTime--------------
				if (0 <= currentTime - SugeryFinishedTime && currentTime - SugeryFinishedTime <= LungUpperChargeTime)
				{
					ServoSwitch("ServoUpper", "On");
					Serial.println("LungUpper On");
				}

				//------------------關閉筏--------------
				else
				{
					ServoSwitch("ServoUpper", "Off");
					Serial.println("LungUpper Off");
				}
			}

			//------------------呼吸模式下，上肺葉開關狀態--------------
			else
			{
				//------------------開筏至LungUpperOnVentTime--------------
				if (0 <= currentTime - previousTime && currentTime - previousTime <= LungUpperOnVentTime)
				{
					ServoSwitch("ServoUpper", "On");
					Serial.println("LungUpper On");
				}

				//------------------關閉筏至LungUpperOffVentAndOnIntakeTime--------------
				else if (LungUpperOnVentTime <= currentTime - previousTime && currentTime - previousTime <= LungUpperOffVentAndOnIntakeTime)
				{
					ServoSwitch("ServoUpper", "Off");
					Serial.println("LungUpper Off");
				}

				//------------------開筏至LungUpperOffIntakeTime--------------
				else if (LungUpperOffVentAndOnIntakeTime <= currentTime - previousTime && currentTime - previousTime <= LungUpperOffIntakeTime)
				{
					ServoSwitch("ServoUpper", "On");
					Serial.println("LungUpper On");
				}

				//------------------關閉筏至previousTime重製--------------
				else
				{
					ServoSwitch("ServoUpper", "Off");
					Serial.println("LungUpper Off");
				}
			}
		}
		else {
			ServoSwitch("ServoUpper", "Off");
			Serial.println("LungUpper Switch Off");
		}

		//------------------如果中肺葉開關開啟，中肺葉氣閥隨著不同的狀態開關閥--------------
		if (LungMiddleSwPin) {
			//------------------充氣準備模式下，中肺葉開關狀態--------------
			if (NormalModeInitialBlowFlag)
			{
				//------------------開筏至LungMiddleChargeTime--------------
				if (0 <= currentTime - SugeryFinishedTime && currentTime - SugeryFinishedTime <= LungMiddleChargeTime)
				{
					ServoSwitch("ServoMiddle", "On");
					Serial.println("LungMiddle On");
				}

				//------------------關閉筏-------------
				else
				{
					ServoSwitch("ServoMiddle", "Off");
					Serial.println("LungMiddle Off");
				}
			}

			//------------------呼吸模式下，中肺葉開關狀態--------------
			else
			{
				//------------------開筏至LungMiddleOnVentTime--------------
				if (0 <= currentTime - previousTime && currentTime - previousTime <= LungMiddleOnVentTime)
				{
					ServoSwitch("ServoMiddle", "On");
					Serial.println("LungMiddle On");
				}

				//------------------關閉筏至LungMiddleOffVentAndOnIntakeTime--------------
				else if (LungMiddleOnVentTime <= currentTime - previousTime && currentTime - previousTime <= LungMiddleOffVentAndOnIntakeTime)
				{
					ServoSwitch("ServoMiddle", "Off");
					Serial.println("LungMiddle Off");
				}

				//------------------開筏至LungMiddleOffIntakeTime--------------
				else if (LungMiddleOffVentAndOnIntakeTime <= currentTime - previousTime && currentTime - previousTime <= LungMiddleOffIntakeTime)
				{
					ServoSwitch("ServoMiddle", "On");
					Serial.println("LungMiddle On");
				}

				//------------------關閉筏至previousTime重製--------------
				else
				{
					ServoSwitch("ServoMiddle", "Off");
					Serial.println("LungMiddle Off");
				}
			}
		}
		else {
			ServoSwitch("ServoMiddle", "Off");
			Serial.println("LungMiddle Switch Off");
		}

		//------------------如果下肺葉開關開啟，下肺葉氣閥隨著不同的狀態開關閥--------------
		if (LungLowerSwPin) {
			//------------------充氣準備模式下，下肺葉開關狀態--------------
			if (NormalModeInitialBlowFlag)
			{
				//------------------開筏至LungLowChargeTime--------------
				if (0 <= currentTime - SugeryFinishedTime && currentTime - SugeryFinishedTime <= LungLowChargeTime)
				{
					ServoSwitch("ServoLower", "On");
					Serial.println("LungLower On");
				}

				//------------------關閉筏--------------
				else
				{
					ServoSwitch("ServoLower", "Off");
					Serial.println("LungLower Off");
				}
			}

			//------------------呼吸準備模式下，下肺葉開關狀態--------------
			else
			{
				//------------------開筏至LungLowOnVentTime--------------
				if (0 <= currentTime - previousTime && currentTime - previousTime <= LungLowOnVentTime)
				{
					ServoSwitch("ServoLower", "On");
					Serial.println("LungLower On");
				}

				//------------------關閉筏至LungLowOffVentAndOnIntakeTime--------------
				else if (LungLowOnVentTime <= currentTime - previousTime && currentTime - previousTime <= LungLowOffVentAndOnIntakeTime)
				{
					ServoSwitch("ServoLower", "Off");
					Serial.println("LungLower Off");
				}

				//------------------開筏至LungLowOffIntakeTime-------------
				else if (LungLowOffVentAndOnIntakeTime <= currentTime - previousTime && currentTime - previousTime <= LungLowOffIntakeTime)
				{
					ServoSwitch("ServoLower", "On");
					Serial.println("LungLower On");
				}

				//------------------關閉筏至previousTime重製--------------
				else
				{
					ServoSwitch("ServoLower", "Off");
					Serial.println("LungLower Off");
				}
			}
		}
		else {
			ServoSwitch("ServoLower", "Off");
			Serial.println("LungLower Switch Off");
		}

		//------------------先將肺葉充氣至一般大小(2000ML)--------------
		if (NormalModeInitialBlowFlag) {
			//------------------充氣至NormalModePreparationChargeTime(14000ms)--------------
			if (0 <= currentTime - SugeryFinishedTime && currentTime - SugeryFinishedTime <= NormalModePreparationChargeTime)
			{
				ServoSwitch("ServoBreath", "Exhausting");
				MachineState = "NormalModePreparation";
				Serial.println("After Sugery Start Charge");
				Serial.print("SugeryFinishedTime:");
				Serial.println(SugeryFinishedTime);
				previousTime = currentTime;
			}

			//------------------NormalModePreparationChargeTime後關閉NormalModeInitialBlowFlag，令充飽肺葉的動作只在開啟開關時執行1次--------------
			else
			{
				NormalModeInitialBlowFlag = false;
			}
		}

		//------------------等待肺葉完全充氣之後，才開始執行一般呼吸模式--------------
		else if (NormalModeInitialBlowFlag == false)
		{
			Serial.println("Normal Mode Start");
			MachineState = "NormalModeStart";
			//------------------三個肺葉全開呼吸模式--------------
			if (LungState == 7)
			{
				//------------------充氣至NormalModeVentTime--------------
				if (0 <= currentTime - previousTime && currentTime - previousTime <= NormalModeVentTime)
				{
					ServoSwitch("ServoBreath", "Exhausting");
					/*Serial.println("off");*/
				}

				//------------------抽氣至NormalModePeriod--------------
				else if (NormalModeVentTime < currentTime - previousTime && currentTime - previousTime <= NormalModePeriod)
				{
					ServoSwitch("ServoBreath", "Pumping");
					/*Serial.println("on");*/
				}

				//------------------重置週期-------------
				else
				{
					previousTime = currentTime;
				}
			}

			//------------------三個肺葉全關呼吸模式--------------
			else if (LungState == 0)
			{
				ServoSwitch("ServoBreath", "Idling");
				previousTime = currentTime;
			}

			//------------------其餘六種肺葉開關狀態呼吸模式--------------
			else
			{
				//------------------充氣至NormalModeVentTime--------------
				if (0 <= currentTime - previousTime && currentTime - previousTime <= NormalModeVentTime)
				{
					ServoSwitch("ServoBreath", "Exhausting");
					/*Serial.println("off");*/
				}

				//------------------空轉至NormalModePuaseTime--------------
				else if (NormalModeVentTime < currentTime - previousTime && currentTime - previousTime <= NormalModePuaseTime)
				{
					ServoSwitch("ServoBreath", "Idling");
					/*Serial.println("pause");*/
				}

				//------------------抽氣至NormalModePeriod--------------
				else if (NormalModePuaseTime < currentTime - previousTime && currentTime - previousTime <= NormalModePeriod)
				{
					ServoSwitch("ServoBreath", "Pumping");
					/*Serial.println("on");*/
				}

				//------------------重置週期-------------
				else
				{
					previousTime = currentTime;
				}
			}
		}
	}
}